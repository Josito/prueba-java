/*  -----------------------------------------------------------------------------------------------------------------------
    -----------------------------------------------------------------------------------------------------------------------


    * * * * * * * * * * * * * * * *
    *  1. C A L C U L A D O R A   *
    * * * * * * * * * * * * * * * *
    
    Crea un programa que permita realizar sumas, restas, multiplicaciones y divisiones. 

        - El programa debe recibir dos nÃƒÂºmeros (n1, n2).

        - Debe existir una variable que permita seleccionar de alguna forma el tipo de operaciÃƒÂ³n (suma, resta, multiplicaciÃƒÂ³n 
          o divisiÃƒÂ³n).

        - Opcional: agrega una operaciÃƒÂ³n que permita elevar n1 a la potencia n2.


    -----------------------------------------------------------------------------------------------------------------------
    -----------------------------------------------------------------------------------------------------------------------


    * * * * * * * * * * * * * * * * * * * * *
    *  2. D A D O   E L E C T R Ãƒâ€œ N I C O   *
    * * * * * * * * * * * * * * * * * * * * *
     
    Simula el uso de un dado electrÃƒÂ³nico cuyos valores al azar irÃƒÂ¡n del 1 al 6. 
    
        - Crea una variable "totalScore" en la que se irÃƒÂ¡ almacenando la puntuaciÃƒÂ³n total tras cada una de las tiradas. 

        - Una vez alcanzados los 50 puntos el programa se detendrÃƒÂ¡ y se mostrarÃƒÂ¡ un mensaje que indique el fin de la partida.

        - Debes mostrar por pantalla los distintos valores que nos devuelva el dado (nÃƒÂºmeros del 1 al 6) asÃƒÂ­ como el valor de la
          variable "totalScore" tras cada una de las tiradas. 


    -----------------------------------------------------------------------------------------------------------------------
    -----------------------------------------------------------------------------------------------------------------------


    * * * * * * * * * * * * * * * * * * * * * * *
    *  3. ASINCRONIA                            *
    * * * * * * * * * * * * * * * * * * * * * * *

    En este ejercicio se comprobarÃ¡ la competencia de los alumnos en el concepto de asincronÃ­a 
    Se proporcionan 3 archivos  csv separados por comas y se deberÃ¡n bajar asÃ­ncronamente (promises) 
    
    A la salida se juntarÃ¡n los registros de los 3 archivos en un array que serÃ¡ el parÃ¡metro de entrada 
    de la funcion findIPbyName(array, name ,surname) que buscarÃ¡ una entrada en el array y devolverÃ¡ la IP correspondiente

    Una vez hallada la IP ha de mostrarse por pantalla

    para llamar a la funciÃ³n utilizad el nombre Cari Wederell
    -----------------------------------------------------------------------------------------------------------------------
    -----------------------------------------------------------------------------------------------------------------------

*/






    



 function calculadora(n1, n2, signo) {


     if(signo === '+') {
            return (n1 + n2);
        }
        if(signo === '-') {
            return (n1 - n2);
        }
        if(signo === '*') {
            return (n1 * n2);
        }
        if(signo === '/') {
            return(n1 / n2);
        }
        if(signo ==='^') {
            return Math.pow(n1,n2)
        }
    }

    console.log(calculadora(2,3,'^'))
